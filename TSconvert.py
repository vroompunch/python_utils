import os, shutil
from ffmpy import FFmpeg




def get_ts_files(dirName):
    tsFileList = []
    if os.path.exists(dirName):
        for basename in os.listdir(dirName):
            filePath = os.path.join(dirName, basename)
            if os.path.isfile(filePath):
                if basename.lower().endswith('.ts'):
                    tsFileList.append(basename)
    else:
        return
    return tsFileList





def convert(dirName, tsList):

    os.chdir(dirName)

    count = 0

    # create converted directory
    try:
        os.mkdir('converted')
    except Exception as e:
        print(e)

    # create source files directory in converted directory
    try:
        os.mkdir('converted/source')
    except Exception as e:
        print(e)


    # create error files directory in converted directory
    try:
        os.mkdir('converted/error')
    except Exception as e:
        print(e)

    # error file for error tracking
    errorFile = open('converted/errors.txt'.format(dirName), 'a')

    for file in tsList:
        # gets the list of ts files and creates a similarly named mp4 file
        originalFile = file
        mp4File = file[:-3] + '.mp4'

        # creates the ffmpeg object for running the command
        ff = FFmpeg(inputs={originalFile : None}, outputs={"converted/{}".format(mp4File) : ['-c', 'copy']})

        # runs the ffmpeg command
        try:
            ff.run()
            count +=1
            shutil.move(originalFile, 'converted/source/{}'.format(originalFile))
            print("converted {} of {} files".format(count, len(tsList)))


        except Exception as e:
            print("error" + str(e))
            errorFile.write(originalFile + ' ' + str(e) + '\n')
            shutil.move(originalFile, 'converted/error/{}'.format(originalFile))

    errorFile.close()


def ts_convert(dirName):
    tsList =  get_ts_files(dirName)
    convert(dirName, tsList)




if __name__=='__main__':
    #dirName = './test'
    dirName = input('Please enter the directory in which search and convert must be done:\n')
    ts_convert(dirName)