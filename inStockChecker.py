from bs4 import BeautifulSoup
import requests
import os, shutil

if __name__=='__main__':

    # baseDir = '/home/vroom/projects/scratchSpace'
    # os.chdir(baseDir)
    # print(os.getcwd())

    urls=['https://vediherbals.com/collections/cannabis-infused-medicines/products/cannaflam%E2%84%A2']

    for url in urls:
        print ("checking url:" + url)
        response = requests.get(url, headers={'User-Agent': 'Custom'})
        print("response works")
        print(response)

        soup = BeautifulSoup(response.text, 'html.parser')
        print("soup worked")

        # stockOut = soup.findAll("span", {"class" : "out-of-stock"})
        # print (stockOut)
        #print (soup.text)
        stockOut = soup.text.find('Out of stock')

        if stockOut == -1 :
            print("not found, preparing email")

        else:
            print("out of stock")
