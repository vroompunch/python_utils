import os
import hashlib
import shutil

BLOCK_SIZE = 65536

def get_file_list(dirName):
    filenames = []
    if os.path.exists(dirName):
        for basename in os.listdir(dirName):
            if os.path.isfile(os.path.join(dirName,basename)):
                filePath = os.path.join(dirName, basename)
                filenames.append(filePath)
            else:
                print("not a file")
    return filenames


def hash_generator(fileList):
    fileListWithHash = []
    count = 1
    for file in fileList:
        print(count)
        count = count + 1
        fileHash = hashlib.sha512()
        with open(file, 'rb') as f:
            print(file)
            fb = f.read(BLOCK_SIZE)
            while len(fb) > 0 :
                fileHash.update(fb)
                fb = f.read(BLOCK_SIZE)
        fileListWithHash.append((file, fileHash.hexdigest()))
        print (fileHash.hexdigest())
    return fileListWithHash

def detectDupes(fileListWithHash):
    dupes = []
    listSize = len(fileListWithHash)
    fileListWithHash.sort(key = lambda x: x[1])
    for i in range(listSize):
        j=i+1
        if (j < listSize):
            while fileListWithHash[i][1] == fileListWithHash[j][1] :
                dupes.append(fileListWithHash[j])
                j += 1
                if (j >= listSize):
                    break
        i = j
    return dupes


def dupe_mover(dirName, duplicateFiles):
    os.chdir(dirName)

    try:
        os.mkdir('duplicates')
    except OSError as error:
        print(error)

    for file in duplicateFiles:
        shutil.move(file[0], os.path.join(dirName, 'duplicates', os.path.basename(file[0])))

    return


def sorter_deleter(dirName):
    if os.path.exists(dirName):
        fileList = get_file_list(dirName)
        # fileList.sort(reverse=True, key= lambda x: x[1])
        hashedFileList = hash_generator(fileList)
        duplicateFiles = detectDupes(hashedFileList)
        print (duplicateFiles)
        #dupe_mover(dirName, duplicateFiles)
        print('done')




if __name__=='__main__':
    dirName='c:/users/varun/downloads/video'
    #dirName='D:/Video Bkup/Video'
    #dirName = './test'
    sorter_deleter(dirName)