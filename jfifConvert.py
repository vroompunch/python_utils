import os, shutil


def file_convert(dirName):
    fileList = get_jfif_files(dirName)
    convert(dirName,fileList)


def get_jfif_files(dirName):
    FileList = []
    if os.path.exists(dirName):
        for basename in os.listdir(dirName):
            filePath = os.path.join(dirName, basename)
            if os.path.isfile(filePath):
                if basename.lower().endswith('.jfif'):
                    FileList.append(basename)
    else:
        return
    return FileList


def convert(dirName, fileList):
    os.chdir(dirName)

    count = 0

    # create converted directory
    try:
        os.mkdir('JFIFconverted')
    except Exception as e:
        print(e)

    # create source files directory in converted directory
    try:
        os.mkdir('JFIFconverted/source')
    except Exception as e:
        print(e)

    # create error files directory in converted directory
    try:
        os.mkdir('JFIFconverted/error')
    except Exception as e:
        print(e)

    # error file for error tracking
    errorFile = open('JFIFconverted/errors.txt'.format(dirName), 'a')

    for file in fileList:
        # gets the list of ts files and creates a similarly named mp4 file
        originalFile = file
        jpgFile = file[:-4] + '.jpg'


        # runs the rename command
        try:
            shutil.copyfile(originalFile, jpgFile)
            count += 1
            shutil.move(originalFile, 'JFIFconverted/source/{}'.format(originalFile))
            print("converted {} of {} files".format(count, len(fileList)))


        except Exception as e:
            print("error" + str(e))
            errorFile.write(originalFile + ' ' + str(e) + '\n')
            shutil.move(originalFile, 'JFIFconverted/error/{}'.format(originalFile))

    errorFile.close()


if __name__=='__main__':
    # dirName = './test/nn'
    dirName = input('Please enter the directory in which search and convert must be done:\n')
    file_convert(dirName)